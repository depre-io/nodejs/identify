(function () {
    'use strict'
    const assign = require('object-assign')
    const httpResponse = require('http-response')
    const redis = require('redis');

    const cookie = require('jwt-cookie')(
        (req, cb) => {
            cb(null, {key: req.tenant, salt: process.env.COOKIE_SALT})
        }
    )
   
    const url = `${process.env.REDIS_URL}:${process.env.REDIS_PORT || '6379'}`
    const cache = redis.createClient(url);

    cache.on("error", (err) => {
        console.log('Cache error: ' + err)
    })
    cache.on("connect", () => {
        console.log('Cache connected')
    })
    cache.on("reconnecting", (msg) => {
        console.log('Cache reconnecing: ' + JSON.stringify(msg))
    })
    cache.on("warning", (msg) => {
        console.log('Cache warning ' + msg)
    })

    const vault = require('redis-vault')({
        cache,
        key: process.env.REDIS_VAULT_KEY,
        salt: process.env.REDIS_VAULT_SALT,
        secret: process.env.REDIS_VAULT_SECRET,
        expiration: process.env.REDIS_VAULT_EXPIRATION,
        algorithm: process.env.REDIS_VAULT_ALGORITHM
    })

    const defaults = {
        tenant: 'DEFAULT'
    }

    function middlewareWrapper(o) {
        // if options are static (either via defaults or custom options passed in), wrap in a function
        let optionsCallback = null
        if (typeof o === 'function') {
            optionsCallback = o
        }
        else {
            optionsCallback = function (req, cb) {
                cb(null, o)
            }
        }

        return function tenantMiddleware(req, res, next) {
            optionsCallback(req, function (err, options) {
                if (err) {
                    next(err)
                }
                else {
                    const o = assign({}, defaults, options)
                    cookie.read(req, res)
                        .then((vaultKey) => {
                            vault.retreive(req, res, vaultKey)
                                .then((vaultValue) => {
                                    req.user = vaultValue
                                    next()
                                })
                                .catch(httpResponse.unauthorized(res))
                        })
                        .catch((err)=>{
                            console.log(err)
                            req.user={roles:[]}
                            next()
                        })
                }
            })
        }
    }

    module.exports = middlewareWrapper
}())
